import urllib2 , json

#User input
print "Entering coordinates.."
lon = raw_input("Lon: ")
lat = raw_input("Lat: ")

#Connection to the webpage..
url = "http://api.openweathermap.org/data/2.5/weather?lat=" + str(lat) + "&lon=" + str(lon) + "&appid=44db6a862fba0b067b1930da0d769e98"
response = urllib2.urlopen(url)
data = response.read()
jsoninfo = json.loads(data)

#Gathering data..
weather = str(jsoninfo["weather"][0]["main"])
temperature = int(jsoninfo["main"]["temp"]) - 273.15

#Output..
if weather == "Rain":
    print "I'm singing in the rain!"

if temperature > 20 :
    print "Nice..."
elif temperature < 5 :
    print "brrrr"